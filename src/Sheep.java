public class Sheep {

   enum Animal {sheep, goat};

   public static void main (String[] param) {
      // for debugging
   }

   public static void reorder (Animal[] animals) {
      int goatCounter = 0;
      int animalsIndex = 0;

      for (Animal animal : animals) {
         if (animal == Animal.goat)
         {
            animals[goatCounter] = Animal.goat;

            if (goatCounter != animalsIndex){
               animals[animalsIndex] = Animal.sheep;
            }
            goatCounter ++;
         }
         animalsIndex ++;
      }

   //   for (int i = goatCounter; i < animals.length; i++) {
   //      animals[i] = Animal.sheep;
      }
}